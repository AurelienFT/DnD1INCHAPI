const axios = require('axios').default;
let tokens_eth = require("./coins_eth.json");
const fs = require('fs');

function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function getTransferDataEth() {
  let data = await axios.get(
    "https://api.covalenthq.com/v1/1/events/address/0x11111112542D85B3EF69AE05771c2dCCff4fAa26/?starting-block=12038853&ending-block=latest&match={%22decoded.name%22:%20%22Swapped%22}&page-size=9999999&key=ckey_6272ec7acf0f459bbc9a90bf707"
  )
  let i = 0;
  let total = 0;
  let users = [];
  let tokensSwapped = {};
  let tokensReceived = {};
  for (let item of data.data.data.items) {
    if (!tokens_eth[item.decoded.params[1].value]) {
      await delay(150);
      let data = await axios.get(`https://api.ethplorer.io/getTokenInfo/${item.decoded.params[1].value}?apiKey=EK-v7eoa-etJTACU-1w7Y1`);
      tokens_eth[item.decoded.params[1].value] = {
        name: data.data.name,
        symbol: data.data.symbol,
        decimals: data.data.decimals,
        price: data.data.price
      }
    }
    if (!tokens_eth[item.decoded.params[2].value]) {
      await delay(150);
      let data = await axios.get(`https://api.ethplorer.io/getTokenInfo/${item.decoded.params[2].value}?apiKey=EK-v7eoa-etJTACU-1w7Y1`);
      tokens_eth[item.decoded.params[2].value] = {
        name: data.data.name,
        symbol: data.data.symbol,
        decimals: data.data.decimals,
        price: data.data.price
      }
    }
    let token1Address = tokens_eth[item.decoded.params[1].value];
    let token2Address = tokens_eth[item.decoded.params[2].value];
    if (token1Address && token1Address.price) {
      total = total + (item.decoded.params[4].value / Math.pow(10, token1Address.decimals) * token1Address.price.rate)
    } else if (token2Address && token2Address.price) {
      total = total + (item.decoded.params[5].value / Math.pow(10, token2Address.decimals) * token2Address.price.rate)
    }
    if (token1Address) {
      let tokenReceived = {
        name: token1Address.name,
        amount: (item.decoded.params[4].value / Math.pow(10, token1Address.decimals)),
        price: 0
      }
      if (token1Address.price) {
        tokenReceived.price = (item.decoded.params[4].value / Math.pow(10, token1Address.decimals) * token1Address.price.rate);
      }
      if (!tokensSwapped[item.decoded.params[1].value])
        tokensSwapped[item.decoded.params[1].value] = tokenReceived;
      else {
        tokensSwapped[item.decoded.params[1].value].amount = tokensSwapped[item.decoded.params[1].value].amount + tokenReceived.amount;
        tokensSwapped[item.decoded.params[1].value].price = tokensSwapped[item.decoded.params[1].value].price + tokenReceived.price;
      }
    }

    if (token2Address) {
      let tokenReceived = {
        name: token2Address.name,
        amount: (item.decoded.params[5].value / Math.pow(10, token2Address.decimals)),
        price: 0
      }
      if (token2Address.price) {
        tokenReceived.price = (item.decoded.params[5].value / Math.pow(10, token2Address.decimals) * token2Address.price.rate);
      }
      if (!tokensReceived[item.decoded.params[2].value])
      tokensReceived[item.decoded.params[2].value] = tokenReceived;
      else {
        tokensReceived[item.decoded.params[2].value].amount = tokensReceived[item.decoded.params[2].value].amount + tokenReceived.amount;
        tokensReceived[item.decoded.params[2].value].price = tokensReceived[item.decoded.params[2].value].price + tokenReceived.price;
      }
    }

    if (!users.includes(item.decoded.params[0].value)) {
      users.push(item.decoded.params[0].value);
    }
    i = i + 1;
  }
  
  let keysSorted = Object.keys(tokensSwapped).sort(function(a,b){return -(tokensSwapped[a].price-tokensSwapped[b].price)})
  let sortedTokensSwapped = [];
  for (let key of keysSorted) {
    sortedTokensSwapped.push({
      address: key,
      name: tokensSwapped[key].name,
      price: tokensSwapped[key].price,
      amount: tokensSwapped[key].amount
    })
  };
  keysSorted = Object.keys(tokensReceived).sort(function(a,b){return -(tokensReceived[a].price-tokensReceived[b].price)})
  let sortedTokensReceived = [];
  for (let key of keysSorted) {
    sortedTokensReceived.push({
      address: key,
      name: tokensReceived[key].name,
      price: tokensReceived[key].price,
      amount: tokensReceived[key].amount
    })
  };
  await fs.writeFileSync('eth_data.json', JSON.stringify({
    total: total,
    usersTotal: users.length,
    tokensSwapped: sortedTokensSwapped.slice(0, 5),
    tokensReceived: sortedTokensReceived.slice(0, 5)
  }));
}

async function getTransferDataBsc() {
  let items = [];
  let tokens_binance = require("./coins_binance.json");
let old_tokens_binance = require("./old_data_binance1.json");
let old_tokens2_binance = require("./old_data_binance2.json");
let old_tokens3_binance = require("./old_data_binance3.json")
let old_tokens4_binance = require("./old_data_binance4.json")
let old_tokens5_binance = require("./old_data_binance5.json")
let old_tokens6_binance = require("./old_data_binance6.json")
let old_tokens7_binance = require("./old_data_binance7.json")
let old_tokens8_binance = require("./old_data_binance8.json");

  // let data = await axios.get(
  //   "https://api.covalenthq.com/v1/56/events/address/0x11111112542D85B3EF69AE05771c2dCCff4fAa26/?starting-block=5500001&ending-block=6000000&match={%22decoded.name%22:%20%22Swapped%22}&page-size=9999999&key=ckey_6272ec7acf0f459bbc9a90bf707"
  // )
  // items = items.concat(data.data.data.items);
  let data = await axios.get(
    "https://api.covalenthq.com/v1/56/events/address/0x11111112542D85B3EF69AE05771c2dCCff4fAa26/?starting-block=6300001&ending-block=6600000&match={%22decoded.name%22:%20%22Swapped%22}&page-size=9999999&key=ckey_6272ec7acf0f459bbc9a90bf707"
  )
  items = items.concat(data.data.data.items);
  data = await axios.get(
    "https://api.covalenthq.com/v1/56/events/address/0x11111112542D85B3EF69AE05771c2dCCff4fAa26/?starting-block=6600001&ending-block=7000000&match={%22decoded.name%22:%20%22Swapped%22}&page-size=9999999&key=ckey_6272ec7acf0f459bbc9a90bf707"
  )
  items = items.concat(data.data.data.items);
  data = await axios.get(
    "https://api.covalenthq.com/v1/56/events/address/0x11111112542D85B3EF69AE05771c2dCCff4fAa26/?starting-block=7000001&ending-block=latest&match={%22decoded.name%22:%20%22Swapped%22}&page-size=9999999&key=ckey_6272ec7acf0f459bbc9a90bf707"
  )
  let before_tokens_binance = old_tokens_binance
                              .concat(old_tokens2_binance)
                              .concat(old_tokens3_binance)
                              .concat(old_tokens4_binance)
                              .concat(old_tokens5_binance)
                              .concat(old_tokens6_binance)
                              .concat(old_tokens7_binance)
                              .concat(old_tokens8_binance)
                              ;
  items = items.concat(data.data.data.items).concat(before_tokens_binance);
  let i = 0;
  let total = 0;
  let users = [];
  let tokensSwapped = {};
  let tokensReceived = {};
  for (let item of items) {
    if (!tokens_binance[item.decoded.params[1].value] || !tokens_binance[item.decoded.params[2].value]) {
      continue;
    }
    let token1Address = tokens_binance[item.decoded.params[1].value];
    let token2Address = tokens_binance[item.decoded.params[2].value];
    if (token1Address && token1Address.price) {
      total = total + (item.decoded.params[4].value / Math.pow(10, token1Address.decimals) * token1Address.price)
    } else if (token2Address && token2Address.price) {
      total = total + (item.decoded.params[5].value / Math.pow(10, token2Address.decimals) * token2Address.price)
    }
    if (!users.includes(item.decoded.params[0].value)) {
      users.push(item.decoded.params[0].value);
    }

    if (token1Address) {
      let tokenReceived = {
        name: token1Address.name,
        amount: (item.decoded.params[4].value / Math.pow(10, token1Address.decimals)),
        price: 0
      }
      if (token1Address.price) {
        tokenReceived.price = (item.decoded.params[4].value / Math.pow(10, token1Address.decimals) * token1Address.price);
      }
      if (!tokensSwapped[item.decoded.params[1].value])
        tokensSwapped[item.decoded.params[1].value] = tokenReceived;
      else {
        tokensSwapped[item.decoded.params[1].value].amount = tokensSwapped[item.decoded.params[1].value].amount + tokenReceived.amount;
        tokensSwapped[item.decoded.params[1].value].price = tokensSwapped[item.decoded.params[1].value].price + tokenReceived.price;
      }
    }

    if (token2Address) {
      let tokenReceived = {
        name: token2Address.name,
        amount: (item.decoded.params[5].value / Math.pow(10, token2Address.decimals)),
        price: 0
      }
      if (token2Address.price) {
        tokenReceived.price = (item.decoded.params[5].value / Math.pow(10, token2Address.decimals) * token2Address.price);
      }
      if (!tokensReceived[item.decoded.params[2].value])
      tokensReceived[item.decoded.params[2].value] = tokenReceived;
      else {
        tokensReceived[item.decoded.params[2].value].amount = tokensReceived[item.decoded.params[2].value].amount + tokenReceived.amount;
        tokensReceived[item.decoded.params[2].value].price = tokensReceived[item.decoded.params[2].value].price + tokenReceived.price;
      }
    }
    i = i + 1;
  }
  let keysSorted = Object.keys(tokensSwapped).sort(function(a,b){return -(tokensSwapped[a].price-tokensSwapped[b].price)})
  let sortedTokensSwapped = [];
  for (let key of keysSorted) {
    sortedTokensSwapped.push({
      address: key,
      name: tokensSwapped[key].name,
      price: tokensSwapped[key].price,
      amount: tokensSwapped[key].amount
    })
  };
  keysSorted = Object.keys(tokensReceived).sort(function(a,b){return -(tokensReceived[a].price-tokensReceived[b].price)})
  let sortedTokensReceived = [];
  for (let key of keysSorted) {
    sortedTokensReceived.push({
      address: key,
      name: tokensReceived[key].name,
      price: tokensReceived[key].price,
      amount: tokensReceived[key].amount
    })
  };
  await fs.writeFileSync('bsc_data.json', JSON.stringify({
    total: total,
    usersTotal: users.length,
    tokensSwapped: sortedTokensSwapped.slice(0, 5),
    tokensReceived: sortedTokensReceived.slice(0, 5)
  }));
}

//getTransferDataEth();
getTransferDataBsc();
