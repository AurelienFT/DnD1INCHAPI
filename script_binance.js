//From curl -X GET "https://api.1inch.exchange/v3.0/56/tokens" -H  "accept: application/json"
const binance_tokens = require("./coins_binance.json");
//From curl -X GET "https://api.pancakeswap.info/api/v2/tokens" -H  "accept: application/json"
const prices_pancake_swap = require("./prices_binance.json");
const fs = require('fs');

let object = {};
for (let tokenAddress of Object.keys(binance_tokens)) {
    let price_pancake = prices_pancake_swap[Object.keys(prices_pancake_swap).find(key => key.toLowerCase() === tokenAddress.toLowerCase())];
    if (price_pancake) {
        let price = price_pancake.price;
        object[tokenAddress] = {
            ...binance_tokens[tokenAddress],
            price: price,
        }
    }
}

fs.writeFileSync('coins_binance.json', JSON.stringify(object));
