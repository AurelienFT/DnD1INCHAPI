const express = require('express')
const app = express()
const port = 3322
const fs = require('fs');
var https = require('https');
var certificate  = fs.readFileSync('/etc/letsencrypt/live/dnd1inch.aurelienfoucault.fr/cert.pem', 'utf8');
var privateKey = fs.readFileSync('/etc/letsencrypt/live/dnd1inch.aurelienfoucault.fr/privkey.pem', 'utf8');

var credentials = {key: privateKey, cert: certificate};
app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/eth/data', (req, res) => {
    fs.readFile('./eth_data.json', (err, json) => {
        let obj = JSON.parse(json);
        res.header("Access-Control-Allow-Origin", "*").json(obj);
    });
})

app.get('/bsc/data', (req, res) => {
    fs.readFile('./bsc_data.json', (err, json) => {
        let obj = JSON.parse(json);
        res.header("Access-Control-Allow-Origin", "*").json(obj);
    });
})

var httpsServer = https.createServer(credentials, app);

httpsServer.listen(port);